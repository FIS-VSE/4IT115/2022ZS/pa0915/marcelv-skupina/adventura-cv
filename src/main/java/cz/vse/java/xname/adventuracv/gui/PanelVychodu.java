package cz.vse.java.xname.adventuracv.gui;

import cz.vse.java.xname.adventuracv.logika.HerniPlan;
import cz.vse.java.xname.adventuracv.logika.Hra;
import cz.vse.java.xname.adventuracv.logika.PrikazJdi;
import cz.vse.java.xname.adventuracv.logika.Prostor;
import cz.vse.java.xname.adventuracv.observer.Observer;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;

import java.util.Collection;
import java.util.Set;

public class PanelVychodu implements Observer {

    private ObservableList<String> observableList = FXCollections.observableArrayList();
    private ListView<String> listView = new ListView<>(observableList);
    private HerniPlan herniPlan;

    public PanelVychodu(Hra hra) {
        herniPlan = hra.getHerniPlan();
        herniPlan.register(this);

        listView.setMaxWidth(150);
        listView.setCellFactory(stringListView -> new ListCell<String>() {
            @Override
            protected void updateItem(String nazev, boolean isEmpty) {
                super.updateItem(nazev, isEmpty);
                if(!isEmpty) {
                    setText(nazev);
                    ImageView iw = new ImageView("/maliny.jpeg");
                    iw.setPreserveRatio(true);
                    iw.setFitWidth(50);
                    setGraphic(iw);
                } else {
                    setText(null);
                    setGraphic(null);
                }
            }
        });
        update();
    }

    private void nastavVychody() {
        observableList.clear();
        Collection<Prostor> vychody = herniPlan.getAktualniProstor().getVychody();
        for (Prostor vychod : vychody) {
            observableList.add(vychod.getNazev());
        }
    }

    public ListView<String> getListView() {
        return listView;
    }

    @Override
    public void update() {
        nastavVychody();
    }
}
