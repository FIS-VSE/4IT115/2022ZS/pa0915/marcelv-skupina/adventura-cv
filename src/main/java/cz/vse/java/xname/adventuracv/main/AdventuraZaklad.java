package cz.vse.java.xname.adventuracv.main;

import cz.vse.java.xname.adventuracv.gui.HraciPlocha;
import cz.vse.java.xname.adventuracv.gui.PanelBatohu;
import cz.vse.java.xname.adventuracv.gui.PanelVychodu;
import cz.vse.java.xname.adventuracv.logika.Hra;
import cz.vse.java.xname.adventuracv.logika.PrikazJdi;
import javafx.animation.Animation;
import javafx.animation.AnimationTimer;
import javafx.animation.RotateTransition;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.scene.web.WebView;
import javafx.util.Duration;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;


public class AdventuraZaklad extends Application {

    ImageView viewKarkulky = new ImageView(
            new Image(
                    AdventuraZaklad.class.getResourceAsStream(
                            "/zdroje/karkulka.png"
                            ), 30.0, 40.0, false, true
            )
    );

    private ImageView viewVlka = new ImageView(
            new Image(AdventuraZaklad.class.getResourceAsStream(
                    "/zdroje/vlk.png"),
                    70.0, 50.0, false, true));


    private final TextArea textArea =  new TextArea();
    private HraciPlocha hraciPlocha;
    private Scene scene;
    private BorderPane borderPane;

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        borderPane = new BorderPane();
        scene = new Scene(borderPane, 600, 500);

        scene.getStylesheets().add(AdventuraZaklad.class.getResource("/zdroje/stylesheet.css").toExternalForm());

        primaryStage.setScene(scene);

        // Přidání Menu
        MenuBar menu = createMenu();
        VBox menuAHraVBox = new VBox(menu, borderPane);
        scene.setRoot(menuAHraVBox);

        pripravPanely(primaryStage);

        animuj();
    }

    private void animuj() {
        polozVlka();
        pohybujVlkem();
        rotujVlka();

        animujKarkulku();
    }

    private void animujKarkulku(){
        // mínus, nula, plus = doleva, stůj na místě, doprava
        AtomicInteger akceleratorX = new AtomicInteger(0);
        AtomicInteger akceleratorY = new AtomicInteger(0); // mínus je nahoru, plus dolu

        AnimationTimer prepocitavacSouradnic = prepocitavacSouradnic(akceleratorX, akceleratorY);
        prepocitavacSouradnic.start();

        nastavAkceleratoryPodleStisknutychKlaves(akceleratorX, akceleratorY);
        resetujAkceleratoryPriPusteniKlaves(akceleratorX, akceleratorY);
    }

    private AnimationTimer prepocitavacSouradnic(AtomicInteger akceleratorX, AtomicInteger akceleratorY) {
        AtomicLong lastUpdateTime = new AtomicLong();
        return new AnimationTimer() {
            @Override
            public void handle(long timestamp) {
                if (lastUpdateTime.get() > 0) {
                    double kolikUbehloCyklu = (timestamp - lastUpdateTime.get()) /
                            10_000_000.0;

                    double souradniceX = viewKarkulky.getTranslateX();
                    double zmenaX = kolikUbehloCyklu * akceleratorX.get();
                    viewKarkulky.setTranslateX(souradniceX + zmenaX);

                    double souradniceY = viewKarkulky.getTranslateY();
                    double zmenaY = kolikUbehloCyklu * akceleratorY.get();
                    viewKarkulky.setTranslateY(souradniceY + zmenaY);
                }
                lastUpdateTime.set(timestamp);
            }
        };
    }

    private void nastavAkceleratoryPodleStisknutychKlaves(AtomicInteger akceleratorX, AtomicInteger akceleratorY) {
        scene.addEventFilter(KeyEvent.KEY_PRESSED, event -> {
            if (event.getCode() == KeyCode.A) {
                akceleratorX.set(-1);
            }
            if (event.getCode() == KeyCode.D) {
                akceleratorX.set(1);
            }
            if (event.getCode() == KeyCode.W) {
                akceleratorY.set(-1);
            }
            if (event.getCode() == KeyCode.S) {
                akceleratorY.set(1);
            }
        });
    }

    private void resetujAkceleratoryPriPusteniKlaves(AtomicInteger akceleratorX, AtomicInteger akceleratorY) {
        scene.setOnKeyReleased(event -> {
            akceleratorX.set(0);
            akceleratorY.set(0);
        });
    }
    private void pohybujVlkem() {
        TranslateTransition translateTransition = new TranslateTransition();
        translateTransition.setNode(viewVlka);
        translateTransition.setByX(-300);
        translateTransition.setByY(200);

        translateTransition.setDuration(Duration.seconds(2));

        translateTransition.setCycleCount(Animation.INDEFINITE);
        translateTransition.setAutoReverse(true);
        translateTransition.play();


        viewVlka.setBlendMode(BlendMode.COLOR_BURN);
    }

    private void rotujVlka() {
        RotateTransition rotateTransition = new RotateTransition();
        rotateTransition.setNode(viewVlka);
        rotateTransition.setByAngle(180);
        rotateTransition.setDuration(Duration.seconds(2));
        rotateTransition.setCycleCount(Animation.INDEFINITE);
        rotateTransition.setAutoReverse(true);
        rotateTransition.play();
    }

    private void polozVlka() {
        AnchorPane planekHry = hraciPlocha.getAnchorPane();
        planekHry.getChildren().add(viewVlka);
        AnchorPane.setTopAnchor(viewVlka, 50.0);
        AnchorPane.setLeftAnchor(viewVlka, 300.0);
    }

    private void pripravPanely(Stage primaryStage) {

        // Co chceme: vytvořit full-duplex komunikaci mezi počítačem a uživatelem
        // Směr: od počítače (tj. Hry) k uživateli
        Hra hra = Hra.getHra();

        textArea.appendText(hra.vratUvitani());
        textArea.setEditable(false);

        borderPane.setCenter(textArea);

        // Směr: od uživatele ke hře
        TextField uzivatelskyVstup = new TextField(); // je vhodnější než TextArea, protože u TextArea Enter dělá něco jiného, zde spustí onAction event. onEnter a onAction jsou u TextFieldu synonyma.
        borderPane.setBottom(uzivatelskyVstup);

        class LokalniTrida {
            private int pocet;
            private String ahoj;
        }

        Object instanceAnonymniTridy = new Object(){
            @Override
            public String toString(){
                return "jsem anonymin trida";
            }
        };

        //function zpracujEnter(){
        //  uzivatelskyVstup.clear();
        //}

        // když se zmáčkne Enter...
        uzivatelskyVstup.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String prikaz = uzivatelskyVstup.getText();
                zpracujPrikaz(prikaz);

                uzivatelskyVstup.clear();

                // DU: neumožnit zadávat další příkazy, pokud hra skončila.
            }
        });

        Label label = new Label("Zadej příkaz: ");
        label.setFont(Font.font("Arial", FontWeight.BLACK, 14.0));

        FlowPane flowPane = new FlowPane();
        borderPane.setBottom(flowPane);

        flowPane.getChildren().addAll(label, uzivatelskyVstup);

        uzivatelskyVstup.requestFocus();

        flowPane.setAlignment(Pos.CENTER);

        // HraciPlocha
        hraciPlocha = new HraciPlocha(hra, viewKarkulky);
        HBox boxHraciPlocha = new HBox();
        boxHraciPlocha.setAlignment(Pos.CENTER);
        boxHraciPlocha.getChildren().add(hraciPlocha.getAnchorPane());
        borderPane.setTop(boxHraciPlocha);

        // Pridani PaneluVychodu
        PanelVychodu panelVychodu = new PanelVychodu(hra);
        borderPane.setRight(panelVychodu.getListView());

        panelVychodu.getListView().setOnMouseClicked(mouseEvent -> {
            String cil = panelVychodu.getListView().getSelectionModel().getSelectedItem();
            if(cil==null) return;
            zpracujPrikaz(PrikazJdi.NAZEV+" "+cil);
        });

        // Pridani PaneluBatohu
        PanelBatohu panelBatohu = new PanelBatohu(hra);
        borderPane.setLeft(panelBatohu.getVBox());


        primaryStage.show();
    }

    private MenuBar createMenu() {
        MenuBar menuBar = new MenuBar();
        Menu souborMenu = new Menu("Soubor");
        Menu napovedaMenu = new Menu("Nápověda");

        ImageView novaHraIkonka = new ImageView(new Image(AdventuraZaklad.class.getResourceAsStream("/zdroje/new.gif")));
        MenuItem novaHraMenuItem = new MenuItem("Nová Hra", novaHraIkonka);
        novaHraMenuItem.setOnAction(event -> start(new Stage()));

        MenuItem konecMenuItem = new MenuItem("Konec");
        konecMenuItem.setOnAction(event -> System.exit(0));

        MenuItem oAplikaciMenuItem = new MenuItem("O Aplikaci");
        MenuItem napovedaMenuItem = new MenuItem("Nápověda");

        novaHraMenuItem.setAccelerator(KeyCombination.keyCombination("Ctrl+N"));

        // oddělovač
        SeparatorMenuItem separatorMenuItem = new SeparatorMenuItem();

        souborMenu.getItems().addAll(novaHraMenuItem, separatorMenuItem, konecMenuItem);
        napovedaMenu.getItems().addAll(oAplikaciMenuItem, napovedaMenuItem);

        oAplikaciMenuItem.setOnAction(event -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
//	... set title, content,...
            alert.setTitle("Java FX Adventura");
            alert.setHeaderText("Header Text - Java FX Adventura");
            alert.setContentText("Verze ZS 2021");
            alert.showAndWait();
        });

        napovedaMenuItem.setOnAction(event -> {
            Stage stage = new Stage();
            stage.setTitle("Napoveda k aplikaci");
            WebView webView = new WebView();
            webView.getEngine().load(AdventuraZaklad.class.getResource
                    ("/zdroje/napoveda.html").toExternalForm());
            stage.setScene(new Scene(webView, 600, 600));
            stage.show();
        });


        menuBar.getMenus().addAll(souborMenu, napovedaMenu);
        return menuBar;
    }


    private void zpracujPrikaz(String prikaz) {
        String lokalniPromennaDrziciOdpovedOdPocitace = Hra.getHra().zpracujPrikaz(prikaz);
        textArea.appendText("\n" + lokalniPromennaDrziciOdpovedOdPocitace + "\n");
    }
}
