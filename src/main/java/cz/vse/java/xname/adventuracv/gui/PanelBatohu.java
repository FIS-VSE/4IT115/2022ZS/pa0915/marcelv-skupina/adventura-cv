package cz.vse.java.xname.adventuracv.gui;

import cz.vse.java.xname.adventuracv.logika.Batoh;
import cz.vse.java.xname.adventuracv.logika.Hra;
import cz.vse.java.xname.adventuracv.observer.Observer;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;

public class PanelBatohu implements Observer {

    private FlowPane flowPane = new FlowPane();
    private Label popisek = new Label("Věci v batohu: ");
    private VBox vBox = new VBox();
    private Batoh batoh;

    public PanelBatohu(Hra hra) {
        this.batoh = hra.getBatoh();
        batoh.register(this);

        vBox.getChildren().addAll(popisek, flowPane);
        vBox.setMaxWidth(100.0);

        update();
    }

    public VBox getVBox() {
        return this.vBox;
    }

    @Override
    public void update() {
        for (String vec : batoh.getMnozinaNazvuVeciVBatohu()) {
            System.out.println(vec);
            Image image = new Image(PanelBatohu.class.getResourceAsStream("/" + vec + ".jpeg"), 100,100, false,false);
            ImageView imageView = new ImageView(image);
            flowPane.getChildren().addAll(imageView);
        }
    }
}
