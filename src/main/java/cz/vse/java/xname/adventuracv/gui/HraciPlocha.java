package cz.vse.java.xname.adventuracv.gui;

import cz.vse.java.xname.adventuracv.logika.HerniPlan;
import cz.vse.java.xname.adventuracv.logika.Hra;
import cz.vse.java.xname.adventuracv.logika.Prostor;
import cz.vse.java.xname.adventuracv.observer.Observer;
import javafx.geometry.Point2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

import java.util.Map;

public class HraciPlocha implements Observer {

    private AnchorPane anchorPane = new AnchorPane();
    //private Circle poziceHrace = new Circle(10.0, Paint.valueOf("red"));
    private ImageView poziceHrace = new ImageView(getClass().getResource("Troll-face.sh.png").toString());
    private HerniPlan herniPlan;

    //začátek řešení pozic hráče bez zásahu do logiky - získáváme souřadnice skrz název prostoru
    private Map<String, Point2D> mapaPozicHrace;


    public HraciPlocha(Hra hra, ImageView viewKarkulky) {
        poziceHrace.setPreserveRatio(true);
        poziceHrace.setFitHeight(80);
        poziceHrace = viewKarkulky;
        herniPlan = hra.getHerniPlan();
        Prostor aktualniProstor = herniPlan.getAktualniProstor();

        herniPlan.register(this);

        update();


        Image image = new Image(HraciPlocha.class.getResourceAsStream("herniPlan.png"), 400.0, 250.0, true, false);
        ImageView imageView = new ImageView(image);

        anchorPane.getChildren().addAll(imageView, poziceHrace);
    }

    public AnchorPane getAnchorPane() {
        return this.anchorPane;
    }

    @Override
    public void update() {
        Prostor aktualniProstor = herniPlan.getAktualniProstor();
        AnchorPane.setLeftAnchor(poziceHrace, aktualniProstor.getX());
        AnchorPane.setTopAnchor(poziceHrace, aktualniProstor.getY());

    }
}
